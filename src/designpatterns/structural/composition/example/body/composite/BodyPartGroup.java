package designpatterns.structural.composition.example.body.composite;

import designpatterns.structural.composition.example.body.parts.IBodyPart;

import java.util.ArrayList;
import java.util.List;

// Composite: BodyPartGroup
public class BodyPartGroup implements IBodyPart {
    private final List<IBodyPart> bodyParts;
    private final String groupName;

    public BodyPartGroup(String groupName) {
        this.groupName = groupName;
        this.bodyParts = new ArrayList<>();
    }

    public void addBodyPart(IBodyPart bodyPart) {
        bodyParts.add(bodyPart);
    }

    public void removeBodyPart(IBodyPart bodyPart) {
        bodyParts.remove(bodyPart);
    }

    public void printStructure() {
        System.out.println(groupName);
        for (IBodyPart bodyPart : bodyParts) {
            bodyPart.printStructure();
        }
    }
}
