package designpatterns.structural.composition.example.body.parts;

public class Arm extends IndividualBodyPart {
    public Arm() {
        super("Arm");
    }
}
