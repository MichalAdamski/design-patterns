package designpatterns.structural.composition.example.body.parts;

public class Leg extends IndividualBodyPart {
    public Leg() {
        super("Leg");
    }
}
