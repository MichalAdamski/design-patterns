package designpatterns.structural.composition.example.body.parts;

// Component interface: BodyPart
public interface IBodyPart {
    void printStructure();
}

