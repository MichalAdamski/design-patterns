package designpatterns.structural.composition.example.body.parts;

// Leaf: IndividualBodyPart
public abstract class IndividualBodyPart implements IBodyPart {
    private final String name;

    protected IndividualBodyPart(String name) {
        this.name = name;
    }

    public void printStructure() {
        System.out.println(name);
    }
}
