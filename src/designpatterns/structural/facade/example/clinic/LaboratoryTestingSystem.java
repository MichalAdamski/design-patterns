package designpatterns.structural.facade.example.clinic;

// Subsystem: LaboratoryTestingSystem
public class LaboratoryTestingSystem {
    LaboratoryTestingSystem(){}
    void runTests() {
        System.out.println("Laboratory tests are running.");
    }
}
