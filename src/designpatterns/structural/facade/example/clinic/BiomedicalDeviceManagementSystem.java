package designpatterns.structural.facade.example.clinic;

// Facade: BiomedicalDeviceManagementSystem
public class BiomedicalDeviceManagementSystem {
    private PatientMonitoringSystem monitoringSystem;
    private MedicationDeliverySystem medicationSystem;
    private LaboratoryTestingSystem testingSystem;

    public BiomedicalDeviceManagementSystem() {
        this.monitoringSystem = new PatientMonitoringSystem();
        this.medicationSystem = new MedicationDeliverySystem();
        this.testingSystem = new LaboratoryTestingSystem();
    }

    public void startTreatment() {
        monitoringSystem.startMonitoring();
        medicationSystem.deliverMedication();
    }

    public void runTests() {
        monitoringSystem.startMonitoring();
        testingSystem.runTests();
    }

    public void stopTreatment() {
        monitoringSystem.stopMonitoring();
    }
}
