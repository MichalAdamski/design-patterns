package designpatterns.structural.facade.example.clinic;

// Subsystem: MedicationDeliverySystem
public class MedicationDeliverySystem {
    MedicationDeliverySystem(){}
    void deliverMedication() {
        System.out.println("Medication delivered to the patient.");
    }
}
