package designpatterns.structural.facade.example.clinic;

// Subsystem: PatientMonitoringSystem
public class PatientMonitoringSystem {
    PatientMonitoringSystem(){}
    void startMonitoring() {
        System.out.println("Patient monitoring system started.");
    }

    void stopMonitoring() {
        System.out.println("Patient monitoring system stopped.");
    }
}

