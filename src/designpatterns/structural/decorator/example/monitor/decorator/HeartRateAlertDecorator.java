package designpatterns.structural.decorator.example.monitor.decorator;

import designpatterns.structural.decorator.example.monitor.IVitalSignsMonitor;

// Concrete Decorator: HeartRateAlertDecorator
public class HeartRateAlertDecorator extends VitalSignsMonitorDecorator {
    public HeartRateAlertDecorator(IVitalSignsMonitor vitalSignsMonitor) {
        super(vitalSignsMonitor);
    }

    public void measure() {
        super.measure();
        System.out.println("Checking heart rate for abnormal readings and raising alerts if necessary.");
    }
}