package designpatterns.structural.decorator.example.monitor.decorator;

import designpatterns.structural.decorator.example.monitor.IVitalSignsMonitor;

// Concrete Decorator: TemperatureAlertDecorator
public class TemperatureAlertDecorator extends VitalSignsMonitorDecorator {
    public TemperatureAlertDecorator(IVitalSignsMonitor vitalSignsMonitor) {
        super(vitalSignsMonitor);
    }

    public void measure() {
        super.measure();
        System.out.println("Checking temperature for abnormal readings and raising alerts if necessary.");
    }
}