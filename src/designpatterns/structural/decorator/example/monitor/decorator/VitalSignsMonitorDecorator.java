package designpatterns.structural.decorator.example.monitor.decorator;

import designpatterns.structural.decorator.example.monitor.IVitalSignsMonitor;

// Decorator: VitalSignsMonitorDecorator
public abstract class VitalSignsMonitorDecorator implements IVitalSignsMonitor {
    protected IVitalSignsMonitor vitalSignsMonitor;

    public VitalSignsMonitorDecorator(IVitalSignsMonitor vitalSignsMonitor) {
        this.vitalSignsMonitor = vitalSignsMonitor;
    }

    public void measure() {
        vitalSignsMonitor.measure();
    }
}
