package designpatterns.structural.decorator.example.monitor;

// Concrete Component: BasicVitalSignsMonitor
public class BasicVitalSignsMonitor implements IVitalSignsMonitor {
    @Override
    public void measure() {
        System.out.println("Basic vital signs measurement.");
    }
}
