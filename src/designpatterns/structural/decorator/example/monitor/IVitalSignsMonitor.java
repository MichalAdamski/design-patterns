package designpatterns.structural.decorator.example.monitor;

// Component interface: VitalSignsMonitor
public interface IVitalSignsMonitor {
    void measure();
}



