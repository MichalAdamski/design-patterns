package designpatterns.behavioral.iterator.example.patient;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

// Aggregate: PatientRecordsCollection
public class PatientRecordsCollection implements Iterable<PatientRecord> {
    private List<PatientRecord> records;

    public PatientRecordsCollection() {
        this.records = new ArrayList<>();
    }

    public void addPatientRecord(PatientRecord record) {
        records.add(record);
    }

    public void removePatientRecord(PatientRecord record) {
        records.remove(record);
    }

    public Iterator<PatientRecord> iterator() {
        return records.iterator();
    }
}

