package designpatterns.behavioral.iterator.example.patient;

// PatientRecord class: Represents the individual patient record
public class PatientRecord {
    private String name;
    private int age;
    // Additional patient information and methods

    public PatientRecord(String name, int age) {
        this.name = name;
        this.age = age;
    }

    // Getters and setters for name, age, and other attributes
}
