package designpatterns.behavioral.iterator.example.patient;

import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;

// Concrete Iterator: PatientRecordsIterator
public class PatientRecordsIterator implements Iterator<PatientRecord> {
    private List<PatientRecord> records;
    private int currentPosition;

    public PatientRecordsIterator(List<PatientRecord> records) {
        this.records = records;
        this.currentPosition = 0;
    }

    public boolean hasNext() {
        return currentPosition < records.size();
    }

    public PatientRecord next() {
        if (hasNext()) {
            PatientRecord record = records.get(currentPosition);
            currentPosition++;
            return record;
        }
        throw new NoSuchElementException("No more patient records.");
    }
}
