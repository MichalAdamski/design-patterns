package designpatterns.behavioral.strategy.example.image;

import designpatterns.behavioral.strategy.example.image.strategies.IImageEnhancementStrategy;

// Context: ImageProcessor
public class ImageProcessor {
    private IImageEnhancementStrategy enhancementStrategy;

    public void setEnhancementStrategy(IImageEnhancementStrategy enhancementStrategy) {
        this.enhancementStrategy = enhancementStrategy;
    }

    public void enhanceImage(String image) {
        if (enhancementStrategy != null) {
            enhancementStrategy.enhanceImage(image);
        } else {
            System.out.println("No enhancement strategy set.");
        }
    }
}
