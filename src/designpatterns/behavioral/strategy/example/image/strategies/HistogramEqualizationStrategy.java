package designpatterns.behavioral.strategy.example.image.strategies;

// Concrete Strategy: HistogramEqualizationStrategy
public class HistogramEqualizationStrategy implements IImageEnhancementStrategy {
    public void enhanceImage(String image) {
        System.out.println("Applying histogram equalization to the image: " + image);
        // Perform histogram equalization algorithm
    }
}
