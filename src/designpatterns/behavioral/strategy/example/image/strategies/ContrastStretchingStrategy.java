package designpatterns.behavioral.strategy.example.image.strategies;

// Concrete Strategy: ContrastStretchingStrategy
public class ContrastStretchingStrategy implements IImageEnhancementStrategy {
    public void enhanceImage(String image) {
        System.out.println("Applying contrast stretching to the image: " + image);
        // Perform contrast stretching algorithm
    }
}
