package designpatterns.behavioral.strategy.example.image.strategies;

// Strategy: ImageEnhancementStrategy
public interface IImageEnhancementStrategy {
    void enhanceImage(String image);
}

