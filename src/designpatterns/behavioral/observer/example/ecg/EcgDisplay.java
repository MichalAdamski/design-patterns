package designpatterns.behavioral.observer.example.ecg;

// Concrete designpatterns.behavioral.observer.ecg.Observer: ECGDisplay
public class EcgDisplay implements IObserver {
    private String name;

    public EcgDisplay(String name) {
        this.name = name;
    }

    public void update(int reading) {
        System.out.println(name + " received new ECG reading: " + reading);
    }
}
