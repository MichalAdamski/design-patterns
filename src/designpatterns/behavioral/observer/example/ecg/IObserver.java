package designpatterns.behavioral.observer.example.ecg;

// Observer: Observer
public interface IObserver {
    void update(int reading);
}
