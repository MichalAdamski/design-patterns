package designpatterns.behavioral.observer.example.ecg;

import java.util.ArrayList;
import java.util.List;

// Subject: ECGSensor
public class EcgSensor {
    private List<IObserver> observers;
    private int latestReading;

    public EcgSensor() {
        this.observers = new ArrayList<>();
    }

    public void attach(IObserver observer) {
        observers.add(observer);
    }

    public void detach(IObserver observer) {
        observers.remove(observer);
    }

    public void notifyObservers() {
        for (IObserver observer : observers) {
            observer.update(latestReading);
        }
    }

    public void setReading(int reading) {
        latestReading = reading;
        notifyObservers();
    }
}

