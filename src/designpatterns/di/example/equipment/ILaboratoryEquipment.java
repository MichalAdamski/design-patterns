package designpatterns.di.example.equipment;

// Dependency: LaboratoryEquipment
public interface ILaboratoryEquipment {
    void performTest(String testName);
}

