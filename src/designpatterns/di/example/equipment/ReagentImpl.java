package designpatterns.di.example.equipment;

// Concrete Dependency: ReagentImpl
public class ReagentImpl implements IReagent {
    public void prepare() {
        System.out.println("Preparing reagents for the test.");
    }
}
