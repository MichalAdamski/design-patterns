package designpatterns.di.example.equipment;

// Test Class: BloodTest
public class BloodTest {
    private ILaboratoryEquipment equipment;
    private IReagent reagent;

    public BloodTest(ILaboratoryEquipment equipment, IReagent reagent) {
        this.equipment = equipment;
        this.reagent = reagent;
    }

    public void runTest() {
        reagent.prepare();
        equipment.performTest("Blood Test");
    }
}
