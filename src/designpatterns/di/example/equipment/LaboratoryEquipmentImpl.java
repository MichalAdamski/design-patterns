package designpatterns.di.example.equipment;

// Concrete Dependency: LaboratoryEquipmentImpl
public class LaboratoryEquipmentImpl implements ILaboratoryEquipment {
    public void performTest(String testName) {
        System.out.println("Performing " + testName + " using laboratory equipment.");
    }
}
