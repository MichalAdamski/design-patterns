package designpatterns.di.example.equipment;

// Dependency: Reagent
public interface IReagent {
    void prepare();
}
