package designpatterns.creational.abstractfactory.example.report;

// Abstract Product: Report
public interface IReport {
    void generate();
}
