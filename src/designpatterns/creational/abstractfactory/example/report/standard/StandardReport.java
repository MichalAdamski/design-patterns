package designpatterns.creational.abstractfactory.example.report.standard;

import designpatterns.creational.abstractfactory.example.report.IReport;

// Concrete Product: Standard Report
public class StandardReport implements IReport {
    StandardReport(){}
    @Override
    public void generate() {
        // Logic for generating a standard report
        System.out.println("Generating a standard report...");
    }
}
