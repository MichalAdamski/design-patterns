package designpatterns.creational.abstractfactory.example.report.standard;

import designpatterns.creational.abstractfactory.example.report.IReport;
import designpatterns.creational.abstractfactory.example.report.IReportFormatter;
import designpatterns.creational.abstractfactory.example.report.IReportAbstractFactory;

// Concrete Factory: Standard Report Factory
public class StandardReportFactory implements IReportAbstractFactory {
    @Override
    public IReport createReport() {
        return new StandardReport();
    }

    @Override
    public IReportFormatter createFormatter() {
        return new PdfReportFormatter();
    }
}