package designpatterns.creational.abstractfactory.example.report.standard;

import designpatterns.creational.abstractfactory.example.report.IReport;
import designpatterns.creational.abstractfactory.example.report.IReportFormatter;

// Concrete Product: PDF Report Formatter
public class PdfReportFormatter implements IReportFormatter {
    PdfReportFormatter(){}
    @Override
    public void format(IReport report) {
        // Logic for formatting a report as PDF
        System.out.println("Formatting the report as PDF...");
    }
}
