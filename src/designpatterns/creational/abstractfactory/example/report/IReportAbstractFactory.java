package designpatterns.creational.abstractfactory.example.report;

// Abstract Factory: Report Abstract Factory
public interface IReportAbstractFactory {
    IReport createReport();

    IReportFormatter createFormatter();
}
