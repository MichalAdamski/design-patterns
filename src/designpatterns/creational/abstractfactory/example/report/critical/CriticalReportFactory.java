package designpatterns.creational.abstractfactory.example.report.critical;

import designpatterns.creational.abstractfactory.example.report.IReport;
import designpatterns.creational.abstractfactory.example.report.IReportFormatter;
import designpatterns.creational.abstractfactory.example.report.IReportAbstractFactory;

// Concrete Factory: Critical Report Factory
public class CriticalReportFactory implements IReportAbstractFactory {
    @Override
    public IReport createReport() {
        return new CriticalReport();
    }

    @Override
    public IReportFormatter createFormatter() {
        return new HtmlReportFormatter();
    }
}