package designpatterns.creational.abstractfactory.example.report.critical;

import designpatterns.creational.abstractfactory.example.report.IReport;

// Concrete Product: Critical Report
public class CriticalReport implements IReport {
    CriticalReport(){}
    @Override
    public void generate() {
        // Logic for generating a critical report
        System.out.println("Generating a critical report...");
    }
}
