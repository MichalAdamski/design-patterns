package designpatterns.creational.abstractfactory.example.report.critical;

import designpatterns.creational.abstractfactory.example.report.IReport;
import designpatterns.creational.abstractfactory.example.report.IReportFormatter;

// Concrete Product: HTML Report Formatter
public class HtmlReportFormatter implements IReportFormatter {
    HtmlReportFormatter(){}
    @Override
    public void format(IReport report) {
        // Logic for formatting a report as HTML
        System.out.println("Formatting the report as HTML...");
    }
}
