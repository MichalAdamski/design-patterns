package designpatterns.creational.abstractfactory.example.report;


// Abstract Product: Report Formatter
public interface IReportFormatter {
    void format(IReport report);
}

