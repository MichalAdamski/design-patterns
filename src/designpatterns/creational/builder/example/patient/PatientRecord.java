package designpatterns.creational.builder.example.patient;

// PatientRecord class
public class PatientRecord {
    private String name;
    private int age;
    private String gender;
    private String medicalHistory;

    PatientRecord(String name, int age, String gender, String medicalHistory) {
        this.name = name;
        this.age = age;
        this.gender = gender;
        this.medicalHistory = medicalHistory;
    }
}
