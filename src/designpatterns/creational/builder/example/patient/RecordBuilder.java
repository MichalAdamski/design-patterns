package designpatterns.creational.builder.example.patient;

public class RecordBuilder implements IRecordBuilder {
    private String name;
    private int age;
    private String gender;
    private String medicalHistory;

    public RecordBuilder(String name) {
        this.name = name;
    }

    @Override
    public RecordBuilder setAge(int age) {
        this.age = age;
        return this;
    }

    @Override
    public RecordBuilder setGender(String gender) {
        this.gender = gender;
        return this;
    }

    @Override
    public RecordBuilder setMedicalHistory(String medicalHistory) {
        this.medicalHistory = medicalHistory;
        return this;
    }

    public PatientRecord build() {
        return new PatientRecord(name, age, gender, medicalHistory);
    }
}