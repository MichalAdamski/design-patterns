package designpatterns.creational.builder.example.patient;

public interface IRecordBuilder {
    IRecordBuilder setAge(int age);

    IRecordBuilder setGender(String gender);

    IRecordBuilder setMedicalHistory(String medicalHistory);
}
