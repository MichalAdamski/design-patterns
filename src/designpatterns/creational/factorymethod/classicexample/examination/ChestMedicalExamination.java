package designpatterns.creational.factorymethod.classicexample.examination;

import designpatterns.creational.factorymethod.classicexample.imageprocessing.ChestXRayProcessor;
import designpatterns.creational.factorymethod.classicexample.imageprocessing.IImageProcessor;

public class ChestMedicalExamination extends MedicalExamination {
    @Override
    public byte[] examine(){
        return new byte[10];
    }

    // Factory method - creates instance of specific class sufficient for chest examination
    @Override
    public IImageProcessor createProcessor(){
        return new ChestXRayProcessor();
    }
}
