package designpatterns.creational.factorymethod.classicexample.examination;

import designpatterns.creational.factorymethod.classicexample.imageprocessing.IImageProcessor;

public abstract class MedicalExamination {
    public abstract byte[] examine();

    // Factory method
    public abstract IImageProcessor createProcessor();
}
