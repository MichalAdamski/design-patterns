package designpatterns.creational.factorymethod.classicexample.examination;

import designpatterns.creational.factorymethod.classicexample.imageprocessing.BoneXRayProcessor;
import designpatterns.creational.factorymethod.classicexample.imageprocessing.IImageProcessor;

public class BoneMedicalExamination extends MedicalExamination {
    @Override
    public byte[] examine(){
        return new byte[10];
    }

    // Factory method - creates instance of specific class sufficient for bone examination
    @Override
    public IImageProcessor createProcessor(){
        return new BoneXRayProcessor();
    }
}
