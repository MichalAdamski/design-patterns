package designpatterns.creational.factorymethod.classicexample.examination;

import designpatterns.creational.factorymethod.classicexample.imageprocessing.DentalXRayProcessor;
import designpatterns.creational.factorymethod.classicexample.imageprocessing.IImageProcessor;

public class DentalMedicalExamination extends MedicalExamination {
    @Override
    public byte[] examine(){
        return new byte[10];
    }

    // Factory method - creates instance of specific class sufficient for dental examination
    @Override
    public IImageProcessor createProcessor(){
        return new DentalXRayProcessor();
    }
}
