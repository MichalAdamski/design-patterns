package designpatterns.creational.factorymethod.classicexample.imageprocessing;

public interface IImageProcessor {
    void processImage(byte[] image);
}
