package designpatterns.creational.factorymethod.classicexample.imageprocessing;

public class BoneXRayProcessor implements IImageProcessor {

    @Override
    public void processImage(byte[] image) {
        System.out.println("Processing Bone XRay image");
    }
}
