package designpatterns.creational.factorymethod.classicexample.imageprocessing;

public class DentalXRayProcessor implements IImageProcessor {

    @Override
    public void processImage(byte[] image) {
        System.out.println("Processing Dental XRay image");
    }
}
