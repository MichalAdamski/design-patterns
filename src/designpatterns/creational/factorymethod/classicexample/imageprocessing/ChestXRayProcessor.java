package designpatterns.creational.factorymethod.classicexample.imageprocessing;

public class ChestXRayProcessor implements IImageProcessor {

    @Override
    public void processImage(byte[] image) {
        System.out.println("Processing Chest XRay image");
    }
}
