package designpatterns.creational.factorymethod.staticexample;

public class HeartRateSensor {
    private final int sensorId;
    private final String sensorType;

    private HeartRateSensor(int sensorId, String sensorType) {
        this.sensorId = sensorId;
        this.sensorType = sensorType;
    }

    public void measureHeartRate() {
        System.out.println("Measuring heart rate using sensor " + sensorId + " (" + sensorType + ")");
        // ...
    }

    // Static factory method
    public static HeartRateSensor createSensor(int sensorId, String sensorType) {

        if (!sensorType.equals("CHEST_BAND") && !sensorType.equals("WRIST") && !sensorType.equals("SMART_RING") && !sensorType.equals("SMARTPHONE")) {
            return null;
        }

        return new HeartRateSensor(sensorId, sensorType);
    }
}